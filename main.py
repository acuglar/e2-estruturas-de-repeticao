def corresponding_parenthesis(text):
    l = list(text)

    for i in l:
        if '(' in l and ')' in l:
            l.pop(l.index('('))
            l.pop(l.index(')'))

    result = ''.join(l)

    return result


def remove_more_than_two_repetitions(text):
    s = ''

    for i in range(len(text)):
        if i >= 2:        
            if text[i] == text[i - 1] == text[i - 2]:
                continue
            else:
                s += text[i]
        else:
            s += text[i]
    
    return s